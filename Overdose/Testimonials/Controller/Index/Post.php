<?php

namespace Overdose\Testimonials\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\View\Result\PageFactory;
use Overdose\Testimonials\Api\TestimonialsRepositoryInterface;
use Overdose\Testimonials\Model\TestimonialsFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Filesystem;

/**
 * Class Post
 */
class Post extends Action implements HttpPostActionInterface
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var TestimonialsRepositoryInterface
     */
    protected $testimonialsRepository;

    /**
     * @var TestimonialsFactory
     */
    protected $testimonialsFactory;

    /**
     * @var UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var AdapterFactory
     */
    protected $adapterFactory;

    /**
     * Post constructor.
     *
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param TestimonialsRepositoryInterface $testimonialsRepository
     * @param TestimonialsFactory $testimonialsFactory
     * @param UploaderFactory $uploaderFactory
     * @param AdapterFactory $adapterFactory
     * @param Filesystem $filesystem
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        TestimonialsRepositoryInterface $testimonialsRepository,
        TestimonialsFactory $testimonialsFactory,
        UploaderFactory $uploaderFactory,
        AdapterFactory $adapterFactory,
        Filesystem $filesystem
    ) {
        $this->resultPageFactory = $pageFactory;
        $this->testimonialsRepository = $testimonialsRepository;
        $this->testimonialsFactory = $testimonialsFactory;
        $this->uploaderFactory = $uploaderFactory;
        $this->adapterFactory = $adapterFactory;
        $this->filesystem = $filesystem;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->resultRedirectFactory
                ->create()
                ->setPath('*/*/index');
        }

        $resultRedirect = $this->resultRedirectFactory->create();

        $message = $this->testimonialsFactory->create();

        $imagePath = '';

        if ($this->getRequest()->getFiles()->image['name'] != '') {
            try {
                $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'image']);
                $uploaderFactory->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $imageAdapter = $this->adapterFactory->create();
                $uploaderFactory->addValidateCallback(
                    'overdose_testimonials_upload',
                    $imageAdapter,
                    'validateUploadFile'
                );
                $uploaderFactory->setAllowRenameFiles(true);
                $uploaderFactory->setFilesDispersion(true);
                $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
                $destinationPath = $mediaDirectory->getAbsolutePath('overdose_testimonials');
                $result = $uploaderFactory->save($destinationPath);
                if (!$result) {
                    throw new LocalizedException(
                        __('File cannot be saved to path: $1', $destinationPath)
                    );
                }
                $imagePath = $result['file'];
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }

        $message->setTitle($this->getRequest()->getParam('title'));
        $message->setMessage($this->getRequest()->getParam('message'));
        $message->setImage($imagePath);

        $this->testimonialsRepository->save($message);

        return $resultRedirect->setPath('*/*/index', ['_current' => true]);
    }
}
