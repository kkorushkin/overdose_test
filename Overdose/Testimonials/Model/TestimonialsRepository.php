<?php

namespace Overdose\Testimonials\Model;

use Overdose\Testimonials\Api\Data\TestimonialsInterface;
use Overdose\Testimonials\Api\Data\TestimonialsSearchResultInterface;
use Overdose\Testimonials\Model\ResourceModel\TestimonialsResourceModel;
use Overdose\Testimonials\Api\TestimonialsRepositoryInterface;
use Magento\Framework\Exception\StateException;

/**
 * Class TestimonialsRepository
 */
class TestimonialsRepository implements TestimonialsRepositoryInterface
{

    /**
     * @var array
     */
    private $registry = [];

    /**
     * @var TestimonialsResourceModel
     */
    protected $testimonialsResource;

    public function __construct(
        TestimonialsResourceModel $testimonialsResource
    ) {
        $this->testimonialsResource = $testimonialsResource;
    }

    /**
     * @param int $id
     * @return TestimonialsInterface|void
     */
    public function get($id)
    {
        // TODO: Implement get() method.
    }

    /**
     * @param TestimonialsSearchResultInterface $searchCriteria
     * @return TestimonialsSearchResultInterface|void
     */
    public function getList(TestimonialsSearchResultInterface $searchCriteria)
    {
        // TODO: Implement getList() method.
    }

    /**
     * @param TestimonialsInterface $record
     * @return mixed|TestimonialsInterface
     * @throws StateException
     */
    public function save(TestimonialsInterface $record)
    {
        try {
            /** @var Testimonials $record */
            $this->testimonialsResource->save($record);
            $this->registry[$record->getId()] = $this->get($record->getId());
        } catch (\Exception $exception) {
            throw new StateException(__('Unable to save record #%1', $record->getId()));
        }
        return $this->registry[$record->getId()];
    }

    /**
     * @param TestimonialsInterface $record
     * @return mixed|void
     */
    public function delete(TestimonialsInterface $record)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param int $id
     * @return mixed|void
     */
    public function deleteById($id)
    {
        // TODO: Implement deleteById() method.
    }
}
