<?php


namespace Overdose\Testimonials\Model;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Overdose\Testimonials\Model\ResourceModel\Collection\CollectionFactory;

/**
 * Class DataProvider
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var ResourceModel\Collection\Collection
     */
    protected $collection;

    /**
     * DataProvider constructor.
     *
     * @param CollectionFactory $collectionFactory
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        foreach ($items as $post) {
            $this->loadedData[$post->getId()] = $post->getData();
        }
        return $this->loadedData;

        /*return [];*/
    }
}
