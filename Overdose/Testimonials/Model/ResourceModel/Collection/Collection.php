<?php

namespace Overdose\Testimonials\Model\ResourceModel\Collection;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Overdose\Testimonials\Model\Testimonials;
use Overdose\Testimonials\Model\ResourceModel\TestimonialsResourceModel;

/**
 * Class Collection
 */
class Collection extends AbstractCollection
{
    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(Testimonials::class, TestimonialsResourceModel::class);
    }
}

