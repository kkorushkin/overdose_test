<?php

namespace Overdose\Testimonials\Model\ResourceModel;

use Overdose\Testimonials\Api\Data\TestimonialsInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Testimonials
 */
class TestimonialsResourceModel extends AbstractDb
{
    protected function _construct()
    {
        $this->_init(TestimonialsInterface::TABLE, TestimonialsInterface::ID);
    }
}
