<?php

namespace Overdose\Testimonials\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class Index
 */
class Form extends Template
{
    /**
     * Form constructor.
     *
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('testimonials/index/post', ['_secure' => true]);
    }
}
