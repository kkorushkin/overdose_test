<?php

namespace Overdose\Testimonials\Api;

use Overdose\Testimonials\Api\Data\TestimonialsInterface;
use Overdose\Testimonials\Api\Data\TestimonialsSearchResultInterface;

interface TestimonialsRepositoryInterface
{
    /**
     * Get record
     *
     * @param int $id
     * @return TestimonialsInterface
     */
    public function get($id);

    /**
     * Get list
     *
     * @param TestimonialsSearchResultInterface $searchCriteria
     * @return TestimonialsSearchResultInterface
     */
    public function getList(TestimonialsSearchResultInterface $searchCriteria);

    /**
     * Save record
     *
     * @param TestimonialsInterface $record
     * @return  TestimonialsInterface
     */
    public function save(TestimonialsInterface $record);

    /**
     * Delete record
     *
     * @param TestimonialsInterface $record
     * @return mixed
     */
    public function delete(TestimonialsInterface $record);

    /**
     * Delete record by given id
     *
     * @param int $id
     * @return mixed
     */
    public function deleteById($id);
}
