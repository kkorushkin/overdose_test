<?php


namespace Overdose\Testimonials\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Class TestimonialsInterface
 */
interface TestimonialsInterface extends ExtensibleDataInterface
{
    /**
     * @var string
     */
    const TABLE = 'overdose_testimonials';
    const ID = 'id';
    const TITLE = 'title';
    const MESSAGE = 'message';
    const IMAGE = 'image';
    const CREATED_AT = 'created_at';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @param string $message
     */
    public function setMessage($message);

    /**
     * @return string
     */
    public function getImage();

    /**
     * @param string $imageUrl
     */
    public function setImage($imageUrl);

    /**
     * @return \DateTime
     */
    public function getCreatedAt();
}
