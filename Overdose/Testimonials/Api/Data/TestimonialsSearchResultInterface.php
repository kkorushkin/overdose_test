<?php


namespace Overdose\Testimonials\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface TestimonialsSearchResultInterface
 */
interface TestimonialsSearchResultInterface extends SearchResultsInterface
{
    /**
     * Get posts
     *
     * @return TestimonialsInterface[]
     */
    public function getItems();
}
